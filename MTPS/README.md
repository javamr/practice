#使用方法：
	下载项目到本地，运行ToolStart.java。
#默认模板SSMJ:Spring + Spring MVC + MyBatis +JQuer Easy UI


#模板取值结果
	   Tab
		|
		|-objName:驼峰命名(全部单词大写)
		|
		|-objVariableName:驼峰命名(首字母单词小写)
		|
		|-tabName:表名
		|
		|-objLowercaseName:模块包名（全部小写）
		|
		|-primaryKey:主键列
		|	|
		|	|-field：列名
		|	|
		|	|-type：列值类型
		|	|
		|	|-comment：列注释
		|	|
		|	|-attributeName：属性名(首字母单词小写)
		|	|
		|	|-capitalAttributeName:属性名(全部单词大写)
		|	|
		|	|-attributeType：属性值类型
		|
		|-tableColumns:列结构集合
			|
			|-field：列名
			|
			|-type：列值类型
			|
			|-comment：列注释
			|
			|-attributeName：属性名
			|
			|-capitalAttributeName:属性名(全部单词大写)
			|
			|-attributeType：属性值类型
			
	   Config
		|
		|-配置文件myvelocity-config.properties的取值对象