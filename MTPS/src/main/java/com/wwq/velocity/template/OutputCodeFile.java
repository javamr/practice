package com.wwq.velocity.template;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Map.Entry;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

import com.wwq.velocity.dbdetails.MYSQLDbDispose;
import com.wwq.velocity.util.FilePathUtils;
import com.wwq.velocity.util.FileUtils;
import com.wwq.velocity.util.StringUtils;
import com.wwq.velocity.vo.TableObj;

/**
 * 
 * @ClassName: OutputCodeFile
 * @blog: http://blog.csdn.net/javaMr_wwq
 * @author: 纸飞机
 * @version: V0.1 
 * @date 2016年7月8日 下午5:08:06 
 * @Description: 生成代码
 */
public class OutputCodeFile {
	private static OutputCodeFile codeFile = new OutputCodeFile();
	private OutputCodeFile(){}
	public static OutputCodeFile getInstance() throws ClassNotFoundException, SQLException{
		return codeFile;
	}
	
	public void start() throws SQLException{
		for (TableObj tableObj : MYSQLDbDispose.getInstance().getTableObjs()) {
			for (Entry<String, File> vm : TemplateFileInfos.getInstance().vmfiles().entrySet()) {
				oupu(vm.getKey(),tableObj);
			}
		}
	}
	
	private void oupu(String vm,TableObj tableObj ){
		VelocityContext ctx  = TemplateFileInfos.getInstance().fillTempl(tableObj);
		Template t = TemplateFileInfos.getInstance().velocityEngine().getTemplate(vm);
		String outPath = FilePathUtils.replace(vm, tableObj);
		PrintWriter fileWriter = null;
		try {
			if (!StringUtils.isEmpty(outPath)) {
				if(!new File(outPath).exists()){
					FileUtils.createFiles(outPath);
				}
				
				fileWriter = new PrintWriter(new File(outPath));
				t.merge(ctx, fileWriter);
				//fileWriter.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if (fileWriter!=null) {
				fileWriter.close();
			}
		}
	}
	
}
