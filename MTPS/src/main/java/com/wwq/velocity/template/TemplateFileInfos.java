package com.wwq.velocity.template;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;

import com.wwq.velocity.dbdetails.MYSQLDbDispose;
import com.wwq.velocity.util.DateUtils;
import com.wwq.velocity.util.FilePathUtils;
import com.wwq.velocity.util.FileUtils;
import com.wwq.velocity.util.PropertiesUtils;
import com.wwq.velocity.vo.TableObj;

/**
 * 
 * @ClassName: TemplateInfos
 * @blog: http://blog.csdn.net/javaMr_wwq
 * @author: 纸飞机
 * @version: V0.1 
 * @date 2016年7月8日 下午4:18:00 
 * @Description: 读取模板文件
 */
public class TemplateFileInfos {
	private TemplateFileInfos(){}
	private static TemplateFileInfos templateFileInfos = new TemplateFileInfos();
	public static TemplateFileInfos getInstance(){
		return templateFileInfos;
	}
	
	private File vmpath(){
		String classPath = FilePathUtils.classPath(PropertiesUtils.class);
		String velocityPath = PropertiesUtils.getInstance().get("velocity_path");
		return new File(String.format("%s%s", classPath,velocityPath));
	}
	
	public Map<String,File> vmfiles(){
		Map<String, File> files = new HashMap<String, File>();
		File dir = vmpath();
		for (File file : dir.listFiles()) {
			if (FileUtils.isVm(file)) {
				files.put(file.getName(), file);
			}
		}
		return files;
	}
	
	public VelocityEngine velocityEngine(){
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, PropertiesUtils.getVmPath());
		ve.setProperty(RuntimeConstants.INPUT_ENCODING,"UTF-8");
	    ve.setProperty(RuntimeConstants.OUTPUT_ENCODING,"UTF-8");
		ve.init();
		return ve;
	}
	
	public VelocityContext fillTempl(TableObj tab){
		VelocityContext context = new VelocityContext();
		context.put("Tab", tab);
		context.put("Config", PropertiesUtils.getInstance().getProperties());
		if ("true".equals(PropertiesUtils.getInstance().getProperties().get("isExtractPartageColumn"))) {
			context.put("partageColumn", MYSQLDbDispose.getInstance().extractPartageColumn());
			context.put("app", this);
		}
		
		context.put("dateUtils", new DateUtils());
		return context;
	}
}
