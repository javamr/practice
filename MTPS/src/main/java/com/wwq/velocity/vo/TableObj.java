package com.wwq.velocity.vo;

import java.util.List;

/**
 * @author 王文强
 * @Version: 0.01
 * @CreateDate: 2016-04-05 
 * @Description:表结构对象
 *
 */
public class TableObj {
//	对象变量名（首字母小写）
	private java.lang.String objVariableName;
//	对象类名（首字母大写）
	private java.lang.String objName;
//	模块包名（全部小写）
	private java.lang.String objLowercaseName;
//	表名
	private java.lang.String tabName;
//	列集合
	private List<TableColumns> tableColumns;
//	主键列
	private TableColumns primaryKey;
//	表注释
	private java.lang.String comment;
	
	public TableColumns getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(TableColumns primaryKey) {
		this.primaryKey = primaryKey;
	}
	public java.lang.String getObjVariableName() {
		return objVariableName;
	}
	public void setObjVariableName(java.lang.String objVariableName) {
		this.objVariableName = objVariableName;
	}
	public java.lang.String getObjName() {
		return objName;
	}
	public void setObjName(java.lang.String objName) {
		this.objName = objName;
	}
	public java.lang.String getTabName() {
		return tabName;
	}
	public void setTabName(java.lang.String tabName) {
		this.tabName = tabName;
	}
	public List<TableColumns> getTableColumns() {
		return tableColumns;
	}
	public void setTableColumns(List<TableColumns> tableColumns) {
		this.tableColumns = tableColumns;
	}
	public java.lang.String getObjLowercaseName() {
		return objLowercaseName;
	}
	public void setObjLowercaseName(java.lang.String objLowercaseName) {
		this.objLowercaseName = objLowercaseName;
	}
	public java.lang.String getComment() {
		return comment;
	}
	public void setComment(java.lang.String comment) {
		this.comment = comment;
	}
}
