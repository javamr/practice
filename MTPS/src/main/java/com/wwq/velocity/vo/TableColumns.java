package com.wwq.velocity.vo;

/**
 * @author 王文强
 * @Version: 0.01
 * @CreateDate: 2016-04-05 
 * @Description:列结构对象
 *
 */
public class TableColumns {
	private java.lang.String field;
	private java.lang.String type;
	private java.lang.String comment;
	private java.lang.String attributeName;
	private java.lang.String capitalAttributeName;
	private java.lang.String attributeType;
	private java.lang.String mybatisType;
	private java.lang.Boolean isNull;
	private java.lang.Integer integetSize;
	private java.lang.Integer decimalsSize;
	/** 是否外键 */
	private boolean foreignKey;
	
	public java.lang.String getMybatisType() {
		return mybatisType;
	}
	public void setMybatisType(java.lang.String mybatisType) {
		this.mybatisType = mybatisType;
	}
	public java.lang.String getCapitalAttributeName() {
		return capitalAttributeName;
	}
	public void setCapitalAttributeName(java.lang.String capitalAttributeName) {
		this.capitalAttributeName = capitalAttributeName;
	}
	public java.lang.String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(java.lang.String attributeName) {
		this.attributeName = attributeName;
	}
	public java.lang.String getAttributeType() {
		return attributeType;
	}
	public void setAttributeType(java.lang.String attributeType) {
		this.attributeType = attributeType;
	}
	public java.lang.String getField() {
		return field;
	}
	public void setField(java.lang.String field) {
		this.field = field;
	}
	public java.lang.String getType() {
		return type;
	}
	public void setType(java.lang.String type) {
		this.type = type;
	}
	public java.lang.String getComment() {
		return comment;
	}
	public void setComment(java.lang.String comment) {
		this.comment = comment;
	}
	public java.lang.Boolean getIsNull() {
		return isNull;
	}
	public void setIsNull(java.lang.Boolean isNull) {
		this.isNull = isNull;
	}
	public java.lang.Integer getIntegetSize() {
		return integetSize;
	}
	public void setIntegetSize(java.lang.Integer integetSize) {
		this.integetSize = integetSize;
	}
	public java.lang.Integer getDecimalsSize() {
		return decimalsSize;
	}
	public void setDecimalsSize(java.lang.Integer decimalsSize) {
		this.decimalsSize = decimalsSize;
	}
	public boolean isForeignKey() {
		return foreignKey;
	}
	public void setForeignKey(boolean foreignKey) {
		this.foreignKey = foreignKey;
	}
	@Override
	public String toString() {
		return String.format("{\"field\":\"%s\",\"type\":\"%s\",\"comment\":\"%s\",\"attributeName\":\"%s\",\"attributeType\":\"%s\"}", getField(),getType()
				,getComment(),getAttributeName(),getAttributeType());
	}
	
	@Override
	public boolean equals(Object obj) {
		return this.field.equals(((TableColumns)obj).getField());
	}
	
}
