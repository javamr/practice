package com.wwq.velocity.dbdetails;

/**
 * 
 * @ClassName: MySQLDbType
 * @blog: http://blog.csdn.net/javaMr_wwq
 * @author: 纸飞机
 * @version: V0.1 
 * @date 2016年7月8日 下午2:20:08 
 * @Description: MYSQL数据库数据类型
 */
public class MySQLDbType {
/*	#########################  整数     #########################*/
//	1 字节	小整数值		范围		(-128，127)	(0，255)	
	public final static String MYSQL_COL_TYPE_TINYINT = "tinyint";
//	2字节	大整数值		范围		(-32 768，32 767)	(0，65 535)
	public final static String MYSQL_COL_TYPE_SMALLINT = "smallint";
//	3 字节	大整数值		范围		(-8 388 608，8 388 607)	(0，16 777 215)	
	public final static String MYSQL_COL_TYPE_MEDIUMINT = "mediumint";
//	4 字节	大整数值		范围		(-2 147 483 648，2 147 483 647)	(0，4 294 967 295)	
	public final static String MYSQL_COL_TYPE_INT = "int";
//	8 字节	极大整数值	范围		(-9 233 372 036 854 775 808，9 223 372 036 854 775 807)	(0，18 446 744 073 709 551 615)	
	public final static String MYSQL_COL_TYPE_BIGINT = "bigint";
	
/*	#########################  浮点数     #########################*/
//	4 字节	单精度浮点数值	范围		(-3.402 823 466 E+38，1.175 494 351 E-38)，0，(1.175 494 351 E-38，3.402 823 466 351 E+38)	0，(1.175 494 351 E-38，3.402 823 466 E+38)	
	public final static String MYSQL_COL_TYPE_FLOAT = "float";
//	8 字节	双精度浮点数值	范围		(1.797 693 134 862 315 7 E+308，2.225 073 858 507 201 4 E-308)，0，(2.225 073 858 507 201 4 E-308，1.797 693 134 862 315 7 E+308)	0，(2.225 073 858 507 201 4 E-308，1.797 693 134 862 315 7 E+308)
	public final static String MYSQL_COL_TYPE_DOUBLE = "double";
//	DECIMAL(M,D) ，如果M>D，为M+2否则为D+2	小数值	范围		依赖于M和D的值	依赖于M和D的值
	public final static String MYSQL_COL_TYPE_DECIMAL = "decimal";
//	DECIMAL(M,D) ，如果M>D，为M+2否则为D+2	小数值	范围		依赖于M和D的值	依赖于M和D的值
	public final static String MYSQL_COL_TYPE_NUMERIC = "numeric";
	
/*	#########################  日期和时间类型    #########################*/
//	大小：3字节，范围：1000-01-01/9999-12-31，格式：YYYY-MM-DD，用途：日期值
	public final static String MYSQL_COL_TYPE_DATE = "date";	
//	大小：3字节，范围：'-838:59:59'/'838:59:59'，格式：HH:MM:SS，用途：时间值或持续时间
	public final static String MYSQL_COL_TYPE_TIME = "time";
//	大小：1字节，范围：1901/2155，格式：YYYY，用途：年份值
	public final static String MYSQL_COL_TYPE_YEAR = "year";
//	大小：8字节，范围：1000-01-01 00:00:00/9999-12-31 23:59:59，格式：YYYY-MM-DD HH:MM:SS，用途：混合日期和时间值
	public final static String MYSQL_COL_TYPE_DATETIME = "datetime";
//	大小：8字节，范围：1970-01-01 00:00:00/2037 年某时，格式：YYYYMMDD HHMMSS，用途：混合日期和时间值，时间戳
	public final static String MYSQL_COL_TYPE_TIMESTAMP = "timestamp";
	
/*	#########################  字符串    #########################*/
//	大小：0-255字节，用途：定长字符串
	public final static String MYSQL_COL_TYPE_CHAR = "char";
//	大小：0-65535 字节，用途：变长字符串
	public final static String MYSQL_COL_TYPE_VARCHAR = "varchar";	
//	大小：0-255字节，用途：不超过 255 个字符的二进制字符串
	public final static String MYSQL_COL_TYPE_TINYBLOB = "tinyblob";
//	大小：0-255字节，用途：短文本字符串
	public final static String MYSQL_COL_TYPE_TINYTEXT = "tinytext";
//	大小：0-65 535字节，用途：二进制形式的长文本数据
	public final static String MYSQL_COL_TYPE_BLOB = "blob";
//	大小：0-65 535字节，用途：长文本数据
	public final static String MYSQL_COL_TYPE_TEXT = "text";
//	大小：0-16 777 215字节，用途：二进制形式的中等长度文本数据
	public final static String MYSQL_COL_TYPE_MEDIUMBLOB = "mediumblob";
//	大小：0-16 777 215字节，用途：中等长度文本数据
	public final static String MYSQL_COL_TYPE_MEDIUMTEXT = "mediumtext";
//	大小：0-4 294 967 295字节，用途：	二进制形式的极大文本数据
	public final static String MYSQL_COL_TYPE_LONGBLOB = "longblob";
//	大小：0-4 294 967 295字节，用途：极大文本数据
	public final static String MYSQL_COL_TYPE_LONGTEXT = "longtext";
//	大小：0-M个字节的定长字节符串，用途：二进制字符串
	public final static String MYSQL_COL_TYPE_BINARY = "binary";
//	大小：0-M个字节的定长字节符串，用途：二进制字符串
	public final static String MYSQL_COL_TYPE_VARBINARY = "varbinary";
	
/*	#########################  布尔类型    #########################*/
	public final static String MYSQL_COL_TYPE_BOOL = "bool";
	public final static String MYSQL_COL_TYPE_BOOLEAN = "boolean";

/*	#########################  其他    #########################*/
//	用途：保存位字段值
	public final static String MYSQL_COL_TYPE_BIT = "bit";
//	近似数值数据类型
	public final static String MYSQL_COL_TYPE_REAL = "real";
	
/*	#########################  扩展字段    #########################*/
	public final static String MYSQL_COL_TYPE_ENUM = "enum";
	public final static String MYSQL_COL_TYPE_SET = "set";

}
