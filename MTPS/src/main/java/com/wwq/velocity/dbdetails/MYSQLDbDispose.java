package com.wwq.velocity.dbdetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.wwq.velocity.util.Constants;
import com.wwq.velocity.util.JDBCUtils;
import com.wwq.velocity.util.PropertiesUtils;
import com.wwq.velocity.util.TranslationUtil;
import com.wwq.velocity.vo.TableColumns;
import com.wwq.velocity.vo.TableObj;

/**
 * 
 * @ClassName: DbDispose
 * @blog: http://blog.csdn.net/javaMr_wwq
 * @author: 纸飞机
 * @version: V0.1 
 * @date 2016年7月8日 下午2:22:13 
 * @Description: 获取MYSQL表信息
 */
public class MYSQLDbDispose {
	
	private MYSQLDbDispose(){}
	private static MYSQLDbDispose dbDispose = new MYSQLDbDispose();
	public static MYSQLDbDispose getInstance(){
		return dbDispose;
	}
	
	public TableObj tabCols(String table) throws SQLException{
		TableObj obj = new TableObj();
		ResultSet tableRs =  JDBCUtils.getInstance().performQuerySql(String.format(Constants.FIND_MYSQL_TAB_INFO, table));
		if(tableRs.next()){
			obj.setComment(tableRs.getString("Comment"));
			tableRs.close();
		}
		
		ResultSet foreignKeyRs =  JDBCUtils.getInstance().performQuerySql(String.format(Constants.FIND_MYSQL_TAB_FOREIGN_KEY, table));
		ArrayList<String> foreignKeys = new ArrayList<>();
		while(foreignKeyRs.next()){
			foreignKeys.add(foreignKeyRs.getString("COLUMN_NAME"));
		}
		foreignKeyRs.close();
		
		ResultSet rs=  JDBCUtils.getInstance().performQuerySql(String.format(Constants.FIND_MYSQL_TAB_CONG, table));
		obj.setTabName(table);
		obj.setObjVariableName(TranslationUtil.conversion(table));
		obj.setObjName(TranslationUtil.allCapital(table));
		obj.setObjLowercaseName(TranslationUtil.allLowercase(table));
		
		List<TableColumns> columns = new ArrayList<TableColumns>();
		while(rs.next()){
			TableColumns cs = new TableColumns();
			cs.setField(rs.getString("Field"));
			cs.setType(rs.getString("Type"));
			cs.setComment(rs.getString("Comment"));
			cs.setAttributeName(TranslationUtil.conversion(rs.getString("Field")));
			cs.setCapitalAttributeName(TranslationUtil.allCapital(rs.getString("Field")));
			cs.setAttributeType(DbTypeToJavaType.conversionJavaType(rs.getString("Type")));
			cs.setMybatisType(DbTypeToJavaType.conversionMyBataisType(rs.getString("Type")));
			cs.setIsNull("NO".equals(rs.getString("Null")));
			if(cs.getType().indexOf("(")>0){
				String [] sizes = cs.getType().substring(cs.getType().indexOf("(")+1, cs.getType().indexOf(")")).split(",");
				cs.setIntegetSize(Integer.parseInt(sizes[0]));
				if(sizes.length==2){
					cs.setDecimalsSize(Integer.parseInt(sizes[1]));
				}
			}
			/** 如果Key是MUL，那么这个列可能是外键列 */
			if("MUL".equals(rs.getString("Key"))&&foreignKeys.contains(cs.getField())){
				cs.setForeignKey(true);
			}else{
				cs.setForeignKey(false);
			}
			
			if ("PRI".equals(rs.getString("Key"))) {
				obj.setPrimaryKey(cs);
			}else{
				columns.add(cs);
			}
		}
		obj.setTableColumns(columns);
		rs.close();
		return obj;
	}
	
	public TableObj[] tableObjs = null;
	public TableObj[] getTableObjs() throws SQLException{
		String[] driver = PropertiesUtils.getInstance().get("tabs").split(",");
		tableObjs = new TableObj[driver.length];
		for (int i = 0;i<driver.length;i++) {
			tableObjs[i] = tabCols(driver[i]);
		}
		return tableObjs;
	}
	
	public List<TableColumns> extractPartageColumn(){
		if (tableObjs==null||tableObjs.length==0) {
			return null;
		}
		int minIndex = minColumnTable();
		List<TableColumns> partageColumn = new ArrayList<TableColumns>();
		
		for (TableColumns ptc : tableObjs[minIndex].getTableColumns()) {
			int same = 0;
			for (int i = 0; i < tableObjs.length; i++) {
				List<TableColumns> tableColumns = tableObjs[i].getTableColumns();
				for (TableColumns tc : tableColumns) {
					if (ptc.getField().equals(tc.getField())) {
						same++;
					}
				}
			}
			if (same==tableObjs.length) {
				partageColumn.add(ptc);
			}
		}
		return partageColumn;
	}
	
	private int minColumnTable(){
		int min = 0;
		int minIndex = 0;
		for (int i = 0,size=tableObjs.length; i < size; i++) {
			if (tableObjs[0].getTableColumns().size()>min) {
				min = tableObjs[0].getTableColumns().size();
				minIndex = i;
			}
		}
		return minIndex;
	}
}
