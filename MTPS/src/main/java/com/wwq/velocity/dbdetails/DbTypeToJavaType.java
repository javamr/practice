package com.wwq.velocity.dbdetails;

/**
 * 
 * @ClassName: DbTypeToJavaType
 * @blog: http://blog.csdn.net/javaMr_wwq
 * @author: 纸飞机
 * @version: V0.1 
 * @date 2016年7月8日 下午2:20:54 
 * @Description: 将数据库类型转换成JAVA对应的类型
 */
public class DbTypeToJavaType {
	
	/**
	 * Java类型
	 * @param type
	 * @return
	 */
	public static String conversionJavaType(String type){
		if (detectionInt(type)) {
			return "java.lang.Integer";
		}
		if (detectionString(type)) {
			return "java.lang.String";
		}
		if (detectionTime(type)) {
			return "java.sql.Timestamp";
		}
		if (detectionDecimal(type)) {
			return "java.lang.Double";
		}
		if (detectionBol(type)) {
			return "java.lang.Boolean";
		}
		return "java.lang.Object";
	}
	
	/**
	 * MyBatais配置文件类型
	 * @param type
	 * @return
	 */
	public static String conversionMyBataisType(String type){
		if (detectionInt(type)) {
			return "INTEGER";
		}
		if (detectionString(type)) {
			return "VARCHAR";
		}
		if (detectionTime(type)) {
			return "TIMESTAMP";
		}
		if (detectionDecimal(type)) {
			return "DOUBLE";
		}
		if (detectionBol(type)) {
			return "BOOLEAN";
		}
		return "";
	}
	
	/**
	 * 检测数值类型
	 * @param type
	 * @return
	 */
	private static boolean detectionInt(String type){
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_INT)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_TINYINT)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_SMALLINT)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_MEDIUMINT)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_BIGINT)>=0) {
			return true;
		}
		return false;
	}
	
	/**
	 * 检测布尔类型
	 * @param type
	 * @return
	 */
	private static boolean detectionBol(String type){
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_BOOL)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_BOOLEAN)>=0) {
			return true;
		}
		return false;
	}
	
	/**
	 * 检测小数类型
	 * @param type
	 * @return
	 */
	private static boolean detectionDecimal(String type){
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_FLOAT)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_DOUBLE)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_DECIMAL)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_NUMERIC)>=0) {
			return true;
		}
		return false;
	}
	
	/**
	 * 检测日期类型
	 * @param type
	 * @return
	 */
	private static boolean detectionTime(String type){
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_DATE)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_TIME)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_YEAR)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_DATETIME)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_TIMESTAMP)>=0) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 检测数值类型
	 * @param type
	 * @return
	 */
	private static boolean detectionString(String type){
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_CHAR)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_VARCHAR)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_TINYBLOB)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_TINYTEXT)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_BLOB)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_TEXT)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_MEDIUMBLOB)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_MEDIUMTEXT)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_LONGBLOB)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_LONGTEXT)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_BINARY)>=0) {
			return true;
		}
		if (type.indexOf(MySQLDbType.MYSQL_COL_TYPE_VARBINARY)>=0) {
			return true;
		}
		return false;
	}

}
