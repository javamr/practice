package com.wwq.velocity.util;

import com.wwq.velocity.vo.TableObj;

/**
 * @author 王文强
 * @Version: 0.01
 * @CreateDate: 2016-04-05 
 * @Description: 路径帮助类
 *
 */
public class FilePathUtils {
	
	/**
	 * 类的绝对路径
	 * @param c
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static String classPath(Class c){
		return c.getResource("/").getPath();
	}
	
	/**
	 * 替换路径中的特殊字符
	 * @param vm 模板
	 * @param tableObj 表结构
	 * @return
	 */
	public static String replace(String vm,TableObj tableObj){
		String path = PropertiesUtils.getInstance().get(vm);
		if (StringUtils.isEmpty(path)) {
			return null;
		}
		
		String objName = tableObj.getObjName();
		String objLowercaseName = tableObj.getObjLowercaseName();
		return path.replaceAll("##", objName).replace("**", objLowercaseName);
	}
	
}
