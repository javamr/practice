package com.wwq.velocity.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * @author 王文强
 * @Version: 0.01
 * @CreateDate: 2016-04-05 
 * @Description: 配置文件帮助类
 *
 */
public class PropertiesUtils {
	
	private static PropertiesUtils propertiesUtils = null;
	private Properties prop = null;
	public static PropertiesUtils getInstance(){
		if (propertiesUtils==null) {
			propertiesUtils = new PropertiesUtils();
		}
		return propertiesUtils;
	}
	
	private PropertiesUtils(){
		prop = new Properties();
		try {
			String path = String.format("%s%s",FilePathUtils.classPath(this.getClass()),"myvelocity-config.properties");
			FileInputStream fis = new FileInputStream(path);
			InputStreamReader reader = new InputStreamReader(fis,"UTF-8");
			prop.load(reader);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
	}
	
	public String get(String str){
		return (String) prop.get(str);
	}

	public Properties getProperties(){
		return prop;
	}
	
	public static String getVmPath(){
		String classPath = FilePathUtils.classPath(PropertiesUtils.class);
		String velocityPath = PropertiesUtils.getInstance().get("velocity_path");
		return String.format("%s%s", classPath,velocityPath);
	}
	
	public static String getTabs(){
		return PropertiesUtils.getInstance().get("tabs");
	}
	public static String getDbUrl(){
		return PropertiesUtils.getInstance().get("dburl");
	}
}
