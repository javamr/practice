package com.wwq.velocity.util;

/**
 * @author 王文强
 * @Version: 0.01
 * @CreateDate: 2016-04-05 
 * @Description: 字符串帮助类
 *
 */
public class StringUtils {
	
	public static boolean isEmpty(String str){
		return null==str||"".equals(str);
	}
}
