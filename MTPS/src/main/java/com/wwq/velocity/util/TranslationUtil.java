package com.wwq.velocity.util;

/**
 * @author 王文强
 * @Version: 0.01
 * @CreateDate: 2016-04-05 
 * @Description:	表名、列名转换
 *
 */
public class TranslationUtil {
//	TODO 默认间隔，后续改成可配置
	private static java.lang.String defRegex = "_";
	
	/**
	 * 将字符串转换成驼峰命名规则
	 * @param name
	 * @return
	 */
	public static String conversion(String name){
		String[] split = name.split(defRegex);
		StringBuffer sbf = new StringBuffer(split[0]);
		for (int i = 1; i < split.length; i++) {
			sbf.append(firstLetterUppercase(split[i]));
		}
		return sbf.toString();
	}
	
	/**
	 * 将字符串转换成驼峰命名规则
	 * @param name
	 * @return
	 */
	public static String allCapital(String name){
		String[] split = name.split(defRegex);
		StringBuffer sbf = new StringBuffer();
		for (int i = 0; i < split.length; i++) {
			sbf.append(firstLetterUppercase(split[i]));
		}
		return sbf.toString();
	}
	
	/**
	 * 将字符串转换成驼峰命名规则
	 * @param name
	 * @return
	 */
	public static String allLowercase(String name){
		String[] split = name.split(defRegex);
		StringBuffer sbf = new StringBuffer();
		for (int i = 0; i < split.length; i++) {
			sbf.append(split[i]);
		}
		return sbf.toString();
	}
	
	/**
	 * 首字母大写
	 * @param name
	 * @return
	 */
	public static String firstLetterUppercase(String name){
		String first = name.substring(0, 1).toUpperCase();
		String remaining = name.substring(1, name.length());
		return String.format("%s%s", first,remaining);
	}

}
