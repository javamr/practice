package com.wwq.velocity.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @TODO
 * 
 * @ClassName: JDBCUtils
 * @blog: http://blog.csdn.net/javaMr_wwq
 * @author: 纸飞机
 * @version: V0.1 
 * @date 2016年7月8日 下午2:33:29 
 * @Description: JDBC帮助类
 */
public class JDBCUtils {
	private Connection con = null;
	private Statement stmt = null;
	private JDBCUtils(){
		try {
			Class.forName(PropertiesUtils.getInstance().get("jdbc.driver"));
			this.con = DriverManager.getConnection(PropertiesUtils.getInstance().get("jdbc.url"));
			this.stmt = con.createStatement();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	public ResultSet performQuerySql(String sql){
		try {
			return this.stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static JDBCUtils jdbcUtils = new JDBCUtils();
	public static JDBCUtils getInstance(){
		return jdbcUtils;
	}
}
