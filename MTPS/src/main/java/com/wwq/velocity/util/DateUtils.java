package com.wwq.velocity.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @ClassName:  DateUtils   
 * @Description: 日期帮助类
 * @author: 王文强/ta2125022@sina.cn 
 * @date:   2019年4月17日 上午10:48:41   
 *     
 * @Copyright: 2019 www.lonch.com.cn Inc. 保留所有权利.
 */
public class DateUtils {
	
	/**
	 * 返回当前日期
	 * @return
	 */
	public String dateFormat() {
		return new SimpleDateFormat("yyyy年MM月dd日 ahh:mm:ss").format(new Date());
	}
	
	public long newTime(){
		return new Date().getTime();
	}
	
	public String newTime(String pattern){
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(new Date());
	}
	
	public long getTime(String pattern) throws ParseException{
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		String format = dateFormat.format(new Date());
		return dateFormat.parse(format).getTime();
	}
}
