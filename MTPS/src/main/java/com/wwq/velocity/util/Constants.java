package com.wwq.velocity.util;

/**
 * 
 * @ClassName: Constants
 * @blog: http://blog.csdn.net/javaMr_wwq
 * @author: 纸飞机
 * @version: V0.1 
 * @date 2016年7月8日 下午4:12:49 
 * @Description: 程序常量
 */
public class Constants {
	/** MYSQL查询表列信息 */
	public static final String FIND_MYSQL_TAB_CONG = "SHOW FULL COLUMNS FROM %s";
	
	/** MYSQL查询表信息 */
	public static final String FIND_MYSQL_TAB_INFO = "SHOW TABLE STATUS WHERE `name`='%s';";

	/** 查询列的外键信息 */
	public static final String FIND_MYSQL_TAB_FOREIGN_KEY = "SELECT * FROM `information_schema`.`KEY_COLUMN_USAGE` where TABLE_NAME='%s' AND POSITION_IN_UNIQUE_CONSTRAINT IS NOT NULL";
}
