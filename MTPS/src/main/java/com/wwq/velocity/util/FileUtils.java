package com.wwq.velocity.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author 王文强
 * @Version: 0.01
 * @CreateDate: 2016-04-05 
 * @Description: 文件帮助类
 *
 */
public class FileUtils {
	/**
	 * 检测file是否是模板
	 * @param f
	 * @return
	 */
	public static boolean isVm(File f){
		if (f.isDirectory()) {
			return false;
		}
		return isFileSuffix(f,"vm");
	}
	
	/**
	 * 判断文件是否是指定后缀
	 * @param f
	 * @param suf
	 * @return
	 */
	public static boolean isFileSuffix(File f,String suf){
		String name = f.getName();
		int lastIndexOf = name.lastIndexOf(".");
		String suffix = name.substring(lastIndexOf+1);
		return suffix.equals(suf);
	}
	
	/**
	 * 创建文件夹
	 * @param url
	 * @return
	 */
	public static boolean createFiles(String url){
		Path pl = Paths.get(url);
		File files = new File(pl.getParent().toString());
		return files.mkdirs();
	}
	
	/**
	 * 合并文件夹下所有文本文件内容。
	 * @param path
	 * @throws IOException 
	 */
	public static void mergeFilecontents(String path) throws IOException{
		File file = new File(path);
		File[] listFiles = file.listFiles();
		for (File f : listFiles) {
			InputStreamReader  reader = new InputStreamReader(new FileInputStream(f));
			BufferedReader bufferedReader = new BufferedReader(reader);
			String line = null;
			while((line = bufferedReader.readLine())!=null){
				System.out.println(line);
			}
			bufferedReader.close();
		}
	}
	
	public static void main(String[] args) throws IOException {
		mergeFilecontents("C:\\Users\\Administrator\\Desktop\\test\\Adoc");
	}
}
