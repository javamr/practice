package com.wwq.velocity.sample;

import java.sql.SQLException;

import com.wwq.velocity.template.OutputCodeFile;

/**
 * 
 * @ClassName: ToolStart
 * @blog: http://blog.csdn.net/javaMr_wwq
 * @author: 纸飞机
 * @version: V0.1 
 * @date 2016年7月8日 下午5:11:36 
 * @Description: 工具启动入口
 */
public class ToolStart {
	public static void main(String[] args) {
		try {
			OutputCodeFile.getInstance().start();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	
}
